# Changelog

All notable changes to `menu-api` will be documented in this file.

## 0.1.0 - 202X-XX-XX

- initial release
