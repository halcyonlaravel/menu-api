<?php

namespace HalcyonLaravelBoilerplate\Menu\Database\Factories;

use HalcyonLaravelBoilerplate\Menu\Models\MenuNode;

class MenuNodeFactory extends BaseFactory
{
    protected $model = MenuNode::class;

    public function definition()
    {
        return [

        ];
    }
}

