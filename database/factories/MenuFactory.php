<?php

namespace HalcyonLaravelBoilerplate\Menu\Database\Factories;

use HalcyonLaravelBoilerplate\Menu\Models\Menu;

class MenuFactory extends BaseFactory
{
    protected $model = Menu::class;

    public function definition()
    {
        return [

        ];
    }
}

