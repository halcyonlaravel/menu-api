<?php

namespace HalcyonLaravelBoilerplate\Menu\Database\Factories;

use HalcyonLaravelBoilerplate\Menu\Tests\Support\Models\TestModel;

class TestModelFactory extends BaseFactory
{
    protected $model = TestModel::class;

    public function definition()
    {
        return [
            'name' => $this->faker->word,
        ];
    }
}

