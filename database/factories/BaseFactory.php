<?php

namespace HalcyonLaravelBoilerplate\Menu\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

abstract class BaseFactory extends Factory
{
    public static function getTable(): string
    {
        return self::new()->newModel()->getTable();
    }
}
