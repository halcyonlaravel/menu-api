<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;

class ProviderTest extends TestCase
{
    /**
     * @test
     */
    public function lang()
    {
        $this->assertEquals('Same window (_self)', trans('menu-api::a_targets._self'));
    }
}
