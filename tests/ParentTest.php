<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;


use HalcyonLaravelBoilerplate\Menu\Database\Factories\MenuFactory;
use HalcyonLaravelBoilerplate\Menu\Database\Factories\MenuNodeFactory;
use HalcyonLaravelBoilerplate\Menu\MenuFacade;

class ParentTest extends TestCase
{

    /**
     * @throws \ErrorException
     * @test
     */
    public function menu_node_model_has_model_key()
    {
        $configModelKey = 'test-key';

        self::createTesModels(
            $configModelKey,
            ['name' => 'test1'],
            ['name' => 'test2'],
            ['name' => 'test3'],
            ['name' => 'test4'],
        );

        $menuData = [
            'name' => 'name test menu',
//                'description',
//                'model_key',
//                'depth',
//                'enabled',
//                'template',
            'segment' => 'test-segment',
        ];
        $menuNodesData = [
            [
//                'menu_id',
//                'parent_id',
                'label' => 'test label1',
                'url' => '/test1/test1',
//                'a_target',
                'segment' => 'node1',
//                'order_column',
            ],
            [
//                'menu_id',
//                'parent_id',
                'label' => 'test label2',
                'url' => '/test1/test2',
//                'a_target',
                'model_key' => $configModelKey,
                'segment' => 'node2',
//                'order_column',
            ],
            [
//                'menu_id',
//                'parent_id',
                'label' => 'test label3',
                'url' => '/test1/test3',
//                'a_target',
                'segment' => 'node3',
//                'order_column',
            ],
        ];
        MenuFacade::create($menuData, $menuNodesData);

        $this->assertDatabaseCount(MenuFactory::getTable(), 1);
        $this->assertDatabaseCount(MenuNodeFactory::getTable(), 3);

//        $baseUrl = 'https://im-url/qwe/';
//        config(['menu-api.base_url' => $baseUrl]);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $nodes = $menuApi->nodes;
        $this->assertCount(3, $nodes);


        foreach (range(0, 2) as $i) {
            $this->assertEquals($menuNodesData[$i]['label'], $nodes[$i]->label);
//            $this->assertEquals($baseUrl.$menuNodesData[$i]['url'], $nodes[$i]->url);
            $this->assertEquals($menuNodesData[$i]['url'], $nodes[$i]->url);
            $this->assertEquals($menuNodesData[$i]['segment'], $nodes[$i]->segment);
            $this->assertEquals($menuNodesData[$i]['model_key'] ?? null, $nodes[$i]->model_key);
        }

        $this->assertCount(0, $nodes[0]->children);
        $this->assertCount(4, $nodes[1]->children);
        $this->assertCount(0, $nodes[2]->children);

        $children = $nodes[1]->children;
        foreach (range(0, 3) as $i) {
            $n = $i + 1;
            $this->assertEquals("test$n", $children[$i]->label);
        }
    }

    /**
     * @throws \ErrorException
     * @test
     */
    public function menu_node_model_has_children()
    {
        $menuData = [
            'name' => 'name test menu',
            'segment' => 'test-segment',
        ];
        $menuNodesData = [
            [                             // 1
                'label' => 'test label1', // 0
                'url' => 'test1/test1',
                'model_key' => null,
                'segment' => 'node1',
            ],
            [                             // 1
                'label' => 'test label2', // 1
                'url' => 'test2/test2',
                'segment' => 'node2',
                'children' => [
                    [                        // 2
                        'label' => 'child1', // 1 0
                        'url' => 'child1/child1',
                        'segment' => 'child1',
                        'children' => [
                            [                        // 3
                                'label' => 'child3', // 1 0 0
                                'url' => 'child3/child3',
                                'segment' => 'child3',
                            ],
                        ],
                    ],
                    [                        // 2
                        'label' => 'child2', // 1 1
                        'url' => 'child2/child2',
                        'model_key' => null,
                        'segment' => 'child2',
                    ],
                ],
            ],
            [                             // 1
                'label' => 'test label3', // 2
                'url' => 'test3/test3',
                'segment' => 'node3',
            ],
        ];
        MenuFacade::create($menuData, $menuNodesData);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $nodes = $menuApi->nodes;
        $this->assertCount(3, $nodes);

        // depth 1
        $this->assertEquals('test label1', $nodes[0]->label);
        $this->assertCount(0, $nodes[0]->children);

        $this->assertEquals('test label2', $nodes[1]->label);
        $this->assertCount(2, $nodes[1]->children);

        $this->assertEquals('test label3', $nodes[2]->label);
        $this->assertCount(0, $nodes[2]->children);

        // depth 2
        $this->assertEquals('child1', $nodes[1]->children[0]->label);
        $this->assertCount(1, $nodes[1]->children[0]->children);

        $this->assertEquals('child2', $nodes[1]->children[1]->label);
        $this->assertCount(0, $nodes[1]->children[1]->children);

        // depth 3
        $this->assertEquals('child3', $nodes[1]->children[0]->children[0]->label);
        $this->assertCount(0, $nodes[1]->children[0]->children[0]->children);
    }
}
