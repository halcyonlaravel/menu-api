<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;

use HalcyonLaravelBoilerplate\Menu\Database\Factories\MenuFactory;
use HalcyonLaravelBoilerplate\Menu\Database\Factories\MenuNodeFactory;
use HalcyonLaravelBoilerplate\Menu\MenuApi;
use HalcyonLaravelBoilerplate\Menu\MenuFacade;
use HalcyonLaravelBoilerplate\Menu\MenuManager;
use HalcyonLaravelBoilerplate\Menu\Models\Menu;
use HalcyonLaravelBoilerplate\Menu\Models\MenuNode;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MenuTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws \ErrorException
     * @test
     */
    public function with_node()
    {
        $configModelKey = 'test-key';

        self::createTesModels(
            $configModelKey,
            ['name' => 'test1'],
            ['name' => 'test2'],
            ['name' => 'test3'],
            ['name' => 'test4'],
        );

        $menuData = [
            'name' => 'test menu name 1',
            'segment' => 'test-menu-1',
            'model_key' => $configModelKey,
        ];

        MenuFactory::new()
            ->create($menuData);

//        $baseUrl = 'http://my-url.test/blah/';
//        config(
//            [
//                'menu-api.base_url' => $baseUrl,
//            ]
//        );

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $this->assertEquals($menuData['name'], $menuApi->name);

        $this->assertCount(4, $menuApi->nodes);

        foreach ($menuApi->nodes as $k => $nodeApi) {
            $n = $k + 1;
//            $this->assertEquals("test$n", $nodeApi->parent_segment);
            $this->assertEquals("test{$n}_segment", $nodeApi->segment);
            $this->assertEquals("test$n", $nodeApi->label);
            $this->assertEquals(MenuManager::A_TARGETS[1], $nodeApi->a_target);
//            $this->assertEquals("{$baseUrl}test/test$n", $nodeApi->url);
            $this->assertEquals("/test/test$n", $nodeApi->url);
            $this->assertEquals(null, $nodeApi->order);
        }
    }

    /**
     * @test
     */
    public function create_menu()
    {
        MenuFacade::create(
            [
                'name' => 'test',
                'segment' => 'test',
            ],
            []
        );

        $tableMenu = MenuFactory::getTable();
        $tableMenuNode = MenuNodeFactory::getTable();
        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 0);
    }

    /**
     * @throws \ErrorException
     * @test
     */
    public function basic_get_by_segment()
    {
        $menuData = [
            'name' => 'test menu name 1',
            'segment' => 'test-menu-1',
        ];
        $menuNodeData = [
//            'parent_segment' => null,
            'segment' => 'test-node',
            'label' => 'test node',
//            'a_target' => null,
            'url' => 'http://urlTes.test/qwe123',
            'order_column' => null,
        ];
        MenuFactory::new()
            ->has(
                MenuNodeFactory::new($menuNodeData)
            )
            ->create($menuData);

        $tableMenu = MenuFactory::getTable();
        $tableMenuNode = MenuNodeFactory::getTable();

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $this->assertInstanceOf(MenuApi::class, $menuApi);
        $this->assertEquals($menuData['name'], $menuApi->name);

        $this->assertCount(1, $menuApi->nodes);
//        $this->assertEquals($menuNodeData['parent_segment'], $menuApi->nodes[0]->parent_segment);
        $this->assertEquals($menuNodeData['segment'], $menuApi->nodes[0]->segment);
        $this->assertEquals($menuNodeData['label'], $menuApi->nodes[0]->label);
        $this->assertEquals(MenuManager::A_TARGETS[0], $menuApi->nodes[0]->a_target);
        $this->assertEquals($menuNodeData['url'], $menuApi->nodes[0]->url);
        $this->assertEquals(/*$menuNodeData['order_column']*/ 1, $menuApi->nodes[0]->order);
    }

    /**
     * @test
     */
    public function basic_update()
    {
        $menuData = [
            'name' => 'test menu name 1',
            'segment' => 'test-menu-1',
        ];
        $menuNodeData = [
//            'parent_segment' => null,
            'segment' => 'test-node',
            'label' => 'test node',
//            'a_target' => null,
            'url' => 'http://urlTes.test/qwe123',
            'order_column' => null,
        ];
        MenuFactory::new()
            ->has(
                MenuNodeFactory::new($menuNodeData)
            )
            ->create($menuData);

        $tableMenu = MenuFactory::getTable();
        $tableMenuNode = MenuNodeFactory::getTable();

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        /** @var Menu $menu */
        $menu = Menu::first();

        $menuNodeData['id'] = MenuNode::first()->getKey();
        $menuNodeData['segment'] = 'new-segment';
        $menuNodeData['label'] = 'new label';
        $menuNodeData['a_target'] = '_blank';
        $menuNodeData['url'] = 'http://nuew/url-test';

        $nodes = [
            $menuNodeData,
        ];

        MenuFacade::updateNode($menu->getKey(), $nodes);

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $this->assertCount(1, $menuApi->nodes);
//        $this->assertEquals($menuNodeData['parent_segment'], $menuApi->nodes[0]->parent_segment);
        $this->assertEquals($menuNodeData['segment'], $menuApi->nodes[0]->segment);
        $this->assertEquals($menuNodeData['label'], $menuApi->nodes[0]->label);
        $this->assertEquals($menuNodeData['a_target'], $menuApi->nodes[0]->a_target);
        $this->assertEquals($menuNodeData['url'], $menuApi->nodes[0]->url);

        $this->assertEquals($menuNodeData['order_column'], $menuApi->nodes[0]->order);
    }

    /**
     * @test
     */
    public function update_with_new_node()
    {
        $menuData = [
            'name' => 'test menu name 1',
            'segment' => 'test-menu-1',
        ];
        $menuNodeData = [
//            'parent_segment' => null,
            'segment' => 'test-node',
            'label' => 'test node',
//            'a_target' => null,
            'url' => 'http://urlTes.test/qwe123',
            'order_column' => null,
        ];
        MenuFactory::new()
            ->has(
                MenuNodeFactory::new($menuNodeData)
            )
            ->create($menuData);

        $tableMenu = MenuFactory::getTable();
        $tableMenuNode = MenuNodeFactory::getTable();

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        /** @var Menu $menu */
        $menu = Menu::first();

        $menuNodeDataNEW['segment'] = 'new-segment';
        $menuNodeDataNEW['label'] = 'new label';
        $menuNodeDataNEW['a_target'] = '_blank';
        $menuNodeDataNEW['url'] = 'http://nuew/url-test';

        $menuNodeData['id'] = MenuNode::first()->getKey();
        $nodes = [
            $menuNodeData,
            $menuNodeDataNEW,
        ];

        MenuFacade::updateNode($menu->getKey(), $nodes);

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 2);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $this->assertCount(2, $menuApi->nodes);

        // old
//        $this->assertEquals($menuNodeData['parent_segment'], $menuApi->nodes[0]->parent_segment);
        $this->assertEquals($menuNodeData['segment'], $menuApi->nodes[0]->segment);
        $this->assertEquals($menuNodeData['label'], $menuApi->nodes[0]->label);
        $this->assertEquals(MenuManager::A_TARGETS[0], $menuApi->nodes[0]->a_target);
        $this->assertEquals($menuNodeData['url'], $menuApi->nodes[0]->url);
        $this->assertEquals($menuNodeData['order_column'], $menuApi->nodes[0]->order);

        // new
//        $this->assertEquals($menuNodeData['parent_segment'], $menuApi->nodes[0]->parent_segment);
        $this->assertEquals($menuNodeDataNEW['segment'], $menuApi->nodes[1]->segment);
        $this->assertEquals($menuNodeDataNEW['label'], $menuApi->nodes[1]->label);
        $this->assertEquals($menuNodeDataNEW['a_target'], $menuApi->nodes[1]->a_target);
        $this->assertEquals($menuNodeDataNEW['url'], $menuApi->nodes[1]->url);
        $this->assertEquals(1, $menuApi->nodes[1]->order);
    }

    /**
     * @throws \ErrorException
     * @test
     */
    public function deleting_nodes()
    {
        $menuData = [
            'name' => 'test menu name 1',
            'segment' => 'test-menu-1',
        ];
        $menuNodeData = [
//            'parent_segment' => null,
            'segment' => 'test-node',
            'label' => 'test node',
//            'a_target' => null,
            'url' => 'http://urlTes.test/qwe123',
            'order_column' => null,
        ];
        MenuFactory::new()
            ->has(
                MenuNodeFactory::new($menuNodeData)
            )
            ->create($menuData);

        $tableMenu = MenuFactory::getTable();
        $tableMenuNode = MenuNodeFactory::getTable();

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        /** @var Menu $menu */
        $menu = Menu::first();


        MenuFacade::updateNode($menu->getKey(), []);

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 0);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $this->assertCount(0, $menuApi->nodes);
    }

    /**
     * @throws \ErrorException
     * @test
     */
    public function deleting_and_add_nodes()
    {
        $menuData = [
            'name' => 'test menu name 1',
            'segment' => 'test-menu-1',
        ];
        $menuNodeData = [
//            'parent_segment' => null,
            'segment' => 'test-node',
            'label' => 'test node',
//            'a_target' => null,
            'url' => 'http://urlTes.test/qwe123',
            'order_column' => null,
        ];
        MenuFactory::new()
            ->has(
                MenuNodeFactory::new($menuNodeData)
            )
            ->create($menuData);

        $tableMenu = MenuFactory::getTable();
        $tableMenuNode = MenuNodeFactory::getTable();

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        /** @var Menu $menu */
        $menu = Menu::first();
        /** @var MenuNode $menu */
        $menuNode = MenuNode::first();

        $this->assertDatabaseHas(
            MenuNodeFactory::getTable(),
            [$menuNode->getKeyName() => 1]
        );

        $menuNodeDataNEW['segment'] = 'new-segment';
        $menuNodeDataNEW['label'] = 'new label';
        $menuNodeDataNEW['a_target'] = '_blank';
        $menuNodeDataNEW['url'] = 'http://nuew/url-test';

        MenuFacade::updateNode($menu->getKey(), [$menuNodeDataNEW]);

        $this->assertDatabaseCount($tableMenu, 1);
        $this->assertDatabaseCount($tableMenuNode, 1);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $this->assertCount(1, $menuApi->nodes);
        $this->assertDatabaseHas(
            MenuNodeFactory::getTable(),
            [$menuNode->getKeyName() => 2]
        );
    }
}
