<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;

use HalcyonLaravelBoilerplate\Menu\Database\Factories\TestModelFactory;
use HalcyonLaravelBoilerplate\Menu\HasMenuNode;

class UrlTest extends TestCase
{
    /**
     * @test
     */
    public function add_slash_when_not_start_with_slash()
    {
        $api = (new HasMenuNode(TestModelFactory::new()->create()));

        $api->setUrl('');
        $this->assertEquals('/', $api->url);

        $url = 'im-url';
        $api->setUrl($url);
        $this->assertEquals("/$url", $api->url);

        $url = 'im-url/';
        $api->setUrl($url);
        $this->assertEquals("/im-url", $api->url);
    }

    /**
     * @test
     */
    public function actual_url()
    {
        $api = (new HasMenuNode(TestModelFactory::new()->create()));

        $url = 'http://myurl/test-blah';
        $api->setUrl($url);
        $this->assertEquals($url, $api->url);

        $url = 'https://myurl/test-blah';
        $api->setUrl($url);
        $this->assertEquals($url, $api->url);

        $url = 'http://myurl/test-blah/';
        $api->setUrl($url);
        $this->assertEquals('http://myurl/test-blah', $api->url);
    }
}
