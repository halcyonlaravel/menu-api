<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;

use HalcyonLaravelBoilerplate\Menu\MenuFacade;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ExceptionTest extends TestCase
{
    /**
     * @throws \ErrorException
     * @test
     */
    public function invalid_segment()
    {
        $this->expectException(ModelNotFoundException::class);
        MenuFacade::getBySegment('xxx');
    }
}
