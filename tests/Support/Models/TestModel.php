<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests\Support\Models;

use HalcyonLaravelBoilerplate\Menu\Contracts\HasMenuContract;
use HalcyonLaravelBoilerplate\Menu\HasMenuNode;
use HalcyonLaravelBoilerplate\Menu\MenuManager;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property int|null order_column
 */
class TestModel extends Model implements HasMenuContract
{
    public function getTable()
    {
        return 'test_models';
    }

    public function setMenuNode(): HasMenuNode
    {
        return HasMenuNode::create(
            $this,
//            function () {
//                return self::all();
//            }
        )
            ->setSegment($this->name.'_segment')
            ->setLabel($this->name)
            ->setOrder($this->order_column)
            ->setATarget(MenuManager::A_TARGETS[1])
            ->setUrl('test/'.$this->name);
    }
}
