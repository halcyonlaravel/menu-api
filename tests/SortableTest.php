<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;

use HalcyonLaravelBoilerplate\Menu\Database\Factories\MenuFactory;
use HalcyonLaravelBoilerplate\Menu\Database\Factories\MenuNodeFactory;
use HalcyonLaravelBoilerplate\Menu\MenuFacade;

class SortableTest extends TestCase
{

    /**
     * @test
     */
    public function spatie_sortable()
    {
        /**
         * to make sure its spatie/eloquent-sorter installed properly
         */


        $menuData = [
            'name' => 'name test menu',
            'segment' => 'test-segment',
        ];
        $menuNodesData = [
            [
                'label' => 'label1',
                'url' => 'test1/test1',
                'segment' => 'node1',
            ],
            [
                'label' => 'label2',
                'url' => 'test1/test2',
                'model_key' => null,
                'segment' => 'node2',
            ],
            [
                'label' => 'label3',
                'url' => 'test1/test3',
                'segment' => 'node3',
            ],
        ];
        MenuFacade::create($menuData, $menuNodesData);

        $tableNode = MenuNodeFactory::getTable();

        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'label1',
                'order_column' => 1,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'label2',
                'order_column' => 2,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'label3',
                'order_column' => 3,
            ]
        );
    }

    /**
     * @test
     */
    public function custom_sort()
    {
        $menuData = [
            'name' => 'name test menu',
            'segment' => 'test-segment',
        ];
        $menuNodesData = [
            [
                'label' => 'label1_',
                'url' => 'test1/test1',
                'segment' => 'node1',
                'order_column' => 3,
            ],
            [
                'label' => 'label2_',
                'url' => 'test1/test2',
                'model_key' => null,
                'segment' => 'node2',
                'order_column' => 1,
            ],
            [
                'label' => 'label3_',
                'url' => 'test1/test3',
                'segment' => 'node3',
                'order_column' => 2,
            ],
        ];
        MenuFacade::create($menuData, $menuNodesData);

        $tableNode = MenuNodeFactory::getTable();
        $this->assertDatabaseCount($tableNode, 3);

        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'label1_',
                'order_column' => 3,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'label2_',
                'order_column' => 1,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'label3_',
                'order_column' => 2,
            ]
        );

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $nodes = $menuApi->nodes;

        $this->assertEquals('label2_', $nodes[0]->label);
        $this->assertEquals('label3_', $nodes[1]->label);
        $this->assertEquals('label1_', $nodes[2]->label);
    }

    /**
     * @test
     */
    public function sort_children_of_menu_node_models()
    {
        $menuData = [
            'name' => 'name test menu',
            'segment' => 'test-segment',
        ];
        $menuNodesData = [
            [
                'label' => 'parent',
                'url' => 'parent',
                'segment' => 'parent',
                'children' => [
                    [
                        'label' => 'child1',
                        'url' => '1',
                        'model_key' => null,
                        'segment' => 'child1',
                        'order_column' => 3,
                    ],
                    [
                        'label' => 'child2',
                        'url' => '2',
                        'segment' => 'child2',
                        'order_column' => 4,
                    ],
                    [
                        'label' => 'child3',
                        'url' => '3',
                        'model_key' => null,
                        'segment' => 'child3',
                        'order_column' => 2,
                    ],
                    [
                        'label' => 'child4',
                        'url' => '4',
                        'segment' => 'child4',
                        'order_column' => 1,
                    ],
                ],
            ],
        ];
        MenuFacade::create($menuData, $menuNodesData);

        $tableNode = MenuNodeFactory::getTable();
        $this->assertDatabaseCount($tableNode, 5);

        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'child1',
                'order_column' => 3,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'child2',
                'order_column' => 4,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'child3',
                'order_column' => 2,
            ]
        );
        $this->assertDatabaseHas(
            $tableNode,
            [
                'label' => 'child4',
                'order_column' => 1,
            ]
        );

        $menuApi = MenuFacade::getBySegment($menuData['segment']);
//
        $nodes = $menuApi->nodes[0]->children;
//
        $this->assertEquals('child4', $nodes[0]->label);
        $this->assertEquals('child3', $nodes[1]->label);
        $this->assertEquals('child1', $nodes[2]->label);
        $this->assertEquals('child2', $nodes[3]->label);
    }

    /**
     * @test
     */
    public function sort_nodes_model_key()
    {
        $configModelKey = 'test-key';

        self::createTesModels(
            $configModelKey,
            [
                'name' => 'test1',
                'order_column' => 4,
            ],
            [
                'name' => 'test2',
                'order_column' => 1,
            ],
            [
                'name' => 'test3',
                'order_column' => 3,
            ],
            [
                'name' => 'test4',
                'order_column' => 2,
            ],
        );


        $menuData = [
            'name' => 'name test menu',
            'segment' => 'test-segment',
        ];
        $menuNodesData = [
            [
                'label' => 'test label2',
                'url' => 'test1/test2',
                'model_key' => $configModelKey,
                'segment' => 'node2',
            ],
        ];

        MenuFacade::create($menuData, $menuNodesData);


        $this->assertDatabaseCount(MenuFactory::getTable(), 1);
        $this->assertDatabaseCount(MenuNodeFactory::getTable(), 1);

        $menuApi = MenuFacade::getBySegment($menuData['segment']);

        $nodes = $menuApi->nodes;
        $this->assertCount(1, $nodes);

        $this->assertCount(4, $nodes[0]->children);

        $this->assertEquals('test2', $nodes[0]->children[0]->label);
        $this->assertEquals('test4', $nodes[0]->children[1]->label);
        $this->assertEquals('test3', $nodes[0]->children[2]->label);
        $this->assertEquals('test1', $nodes[0]->children[3]->label);
    }
}
