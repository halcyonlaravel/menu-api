<?php

namespace HalcyonLaravelBoilerplate\Menu\Tests;

use CreateMenuApiTable;
use HalcyonLaravelBoilerplate\Menu\Database\Factories\TestModelFactory;
use HalcyonLaravelBoilerplate\Menu\MenuServiceProvider;
use HalcyonLaravelBoilerplate\Menu\Tests\Support\Models\TestModel;
use Illuminate\Database\Schema\Blueprint;
use Orchestra\Testbench\TestCase as Orchestra;
use Spatie\EloquentSortable\EloquentSortableServiceProvider;

class TestCase extends Orchestra
{
    protected static function createTesModels(string $configModelKey, ...$sequence): void
    {
        config(
            [
                'menu-api.models' => array_merge(
                    config('menu-api.models'),
                    [
                        $configModelKey => TestModel::class,
                    ]
                ),
            ]
        );

        TestModelFactory::new()
            ->sequence(...$sequence)
            ->count(count($sequence))
            ->create();
    }

    public function setUp(): void
    {
        parent::setUp();
//
//        Factory::guessFactoryNamesUsing(
//            fn(string $modelName) => 'HalcyonLaravelBoilerplate\\Menu\\Database\\Factories\\'.class_basename(
//                    $modelName
//                ).'Factory'
//        );
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set(
            'database.connections.sqlite',
            [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]
        );

        include_once __DIR__.'/../database/migrations/create_menu_api_table.php.stub';
        (new CreateMenuApiTable())->up();

        $schema = $app['db']->connection()->getSchemaBuilder();
        $schema->create(
            (new TestModel())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->unsignedInteger('order_column')->nullable();
                $table->timestamps();
            }
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            MenuServiceProvider::class,
            EloquentSortableServiceProvider::class,
        ];
    }
}
