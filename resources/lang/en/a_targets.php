<?php

return [
    '_self' => 'Same window (_self)',
    '_blank' => 'New window (_blank)',
    '_parent' => 'Parent window (_parent)',
    '_top' => 'Top window (_top)',
];
