<?php

namespace HalcyonLaravelBoilerplate\Menu;

use ErrorException;
use HalcyonLaravelBoilerplate\Menu\Contracts\HasMenuContract;
use HalcyonLaravelBoilerplate\Menu\Models\Menu;
use HalcyonLaravelBoilerplate\Menu\Models\MenuNode;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class MenuManager
{
    public const A_TARGETS = [
        '_self',
        '_blank',
        '_parent',
        '_top',
    ];

    public static function aTargets(): array
    {
        return self::A_TARGETS;
    }

    public static function create(array $attributes, array $nodes): void
    {
        DB::transaction(
            function () use ($attributes, $nodes) {
                $menu = Menu::create($attributes);

                $ids = [];
                foreach ($nodes as $node) {
                    $id = self::createNode($menu, $node)->getKey();

                    $ids[] = [
                        'id' => $id,
                        'order_column' => $node['order_column'] ?? null,
                    ];
                }
                $ids = collect($ids)
                    ->sortBy('order_column')
                    ->values()
                    ->toArray();
                $toBeSort = [];
                foreach ($ids as $id) {
                    $toBeSort[] = $id['id'];
                }
                MenuNode::setNewOrder($toBeSort);
            }
        );
    }

    /**
     * @param  Menu|int  $menu
     * @param  array  $nodes
     */
    public static function updateNode($menu, array $nodes): void
    {
        DB::transaction(
            function () use ($menu, $nodes) {
                $MENU = new Menu();
                $MENU_NODE = new MenuNode();

                $menuNodeKeyName = $MENU_NODE->getKeyName();

                if (! $menu instanceof Menu) {
                    $menu = Menu::where($MENU->getKeyName(), $menu)->first();
                }

                $missingNodes = $menu->menuNodes->pluck($menuNodeKeyName)
                    ->diff(collect($nodes)->pluck($menuNodeKeyName));

                if ($missingNodes->isNotEmpty()) {
                    $menu->menuNodes()->whereIn($menuNodeKeyName, $missingNodes)->delete();
                }

                foreach ($nodes as $node) {
                    if (isset($node['children']) && filled($node['children'])) {
                        self::updateNode($menu, $node['children']);
                    }

                    if (isset($node[$menuNodeKeyName])) {
                        MenuNode::where(
                            $menuNodeKeyName,
                            $node[$menuNodeKeyName]
                        )->update($node);
                    } else {
                        $menu->menuNodes()->create($node);
                    }
                }
            }
        );
    }

    /**
     * @param  string  $segment
     *
     * @return \HalcyonLaravelBoilerplate\Menu\MenuApi
     * @throws \ErrorException
     */
    public static function getBySegment(string $segment): MenuApi
    {
        $menu = Menu::whereSegment($segment)->firstOrFail();

        $menuApi = new MenuApi();

        $menuApi->name = $menu->name;
        $menuApi->description = $menu->description;

        if (is_null($menu->model_key)) {
            $nodes = self::generateNodesFromRelationship($menu->menuNodes()->parents()->get());
        } else {
            $nodes = self::generateNodesFromConfig(config('menu-api.models.'.$menu->model_key));
        }

        $menuApi->nodes = self::sortMenuNodeApi($nodes);

        return $menuApi;
    }

    /**
     * @param  MenuNodeApi[]  $menuNodeApi
     *
     * @return MenuNodeApi[]
     */
    private static function sortMenuNodeApi(array $menuNodeApi): array
    {
        return collect($menuNodeApi)
            ->sortBy('order')
            ->values()->toArray();
    }

    private static function createNode(Menu $menu, array $attributes): Model
    {
        $node = $menu->menuNodes()->create(Arr::except($attributes, ['children']));

        if (isset($attributes['children']) && filled($attributes['children'])) {
            $ids = [];
            foreach ($attributes['children'] as $child) {
                $child['parent_id'] = $node->getKey();
                $id = self::createNode($menu, $child)->getKey();
                $ids[] = [
                    'id' => $id,
                    'order_column' => $child['order_column'] ?? null,
                ];
            }
            $ids = collect($ids)
                ->sortBy('order_column')
                ->values()
                ->toArray();
            $toBeSort = [];
            foreach ($ids as $id) {
                $toBeSort[] = $id['id'];
            }

            MenuNode::setNewOrder($toBeSort);
        }

        return $node;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Collection|MenuNode[]  $menuNodes
     *
     * @return array
     * @throws \ErrorException
     */
    private static function generateNodesFromRelationship(Collection $menuNodes): array
    {
        $nodes = [];

        foreach ($menuNodes as $menuNode) {
            $nodes[] = self::menuNodeApi($menuNode->setMenuNode(), $menuNode->children);
        }

        return $nodes;
    }

    /**
     * @param  string  $modelClassName
     *
     * @return array
     * @throws \ErrorException
     */
    private static function generateNodesFromConfig(string $modelClassName): array
    {
        $nodes = [];

        if (! (new $modelClassName() instanceof Model)) {
            throw new ErrorException("$modelClassName must extend from ".Model::class);
        }

        foreach (HasMenuNode::fetch($modelClassName) as $model) {
            $nodes[] = self::menuNodeApiFromContract($model);
        }

        return $nodes;
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\Menu\Contracts\HasMenuContract  $hasMenuContract
     *
     * @return \HalcyonLaravelBoilerplate\Menu\MenuNodeApi
     * @throws \ErrorException
     */
    private static function menuNodeApiFromContract(HasMenuContract $hasMenuContract): MenuNodeApi
    {
        return self::menuNodeApi($hasMenuContract->setMenuNode());
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\Menu\HasMenuNode  $menuNode
     * @param  array  $children
     *
     * @return \HalcyonLaravelBoilerplate\Menu\MenuNodeApi
     * @throws \ErrorException
     */
    private static function menuNodeApi(HasMenuNode $menuNode, $children = null): MenuNodeApi
    {
        $menuNodeApi = MenuNodeApi::copy($menuNode);

        if (filled($children)) {
            $_children = [];
            foreach ($children as $child) {
                $_children[] = self::menuNodeApi($child->setMenuNode(), $child->children);
            }
            $menuNodeApi->children = $_children;
        } elseif (
            ! is_null($menuNodeApi->model_key) &&
            ! is_null($c = config('menu-api.models.'.$menuNodeApi->model_key))
        ) {
            $menuNodeApi->children = self::generateNodesFromConfig($c);
        }

        if (filled($menuNodeApi->children)) {
            $menuNodeApi->children = self::sortMenuNodeApi($menuNodeApi->children);
        }

        return $menuNodeApi;
    }

}
