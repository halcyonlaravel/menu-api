<?php

namespace HalcyonLaravelBoilerplate\Menu\Contracts;

use HalcyonLaravelBoilerplate\Menu\HasMenuNode;

interface HasMenuContract
{
    public function setMenuNode(): HasMenuNode;
}
