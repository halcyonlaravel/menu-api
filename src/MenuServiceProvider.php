<?php

namespace HalcyonLaravelBoilerplate\Menu;

use HalcyonLaravelBoilerplate\Menu\Commands\MenuCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class MenuServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('menu-api')
            ->hasTranslations()
            ->hasConfigFile()
//            ->hasViews()
            ->hasMigration('create_menu_api_table')
            ->hasCommand(MenuCommand::class);
    }
}
