<?php

namespace HalcyonLaravelBoilerplate\Menu;

use JsonSerializable;

class MenuNodeApi implements JsonSerializable
{
    public ?string $parent_segment = null;
    public string $segment;
    public string $label;
    public string $a_target;
    public string $url;
    public ?int $order = null;
    public ?string $model_key = null;

    /**
     * @var \HalcyonLaravelBoilerplate\Menu\MenuNodeApi[]
     */
    public array $children = [];

    public function __construct()
    {
        $this->a_target = MenuManager::A_TARGETS[0];
    }

//    protected static function addBaseUrl($uri): string
//    {
//        if (Str::startsWith($uri, '/')) {
//            $uri = substr($uri, 1);
//        }
//
//        $baseUrl = trim(config('menu-api.base_url'), '/');
//
//        return trim("$baseUrl/$uri", '/');
//    }

    public static function copy(HasMenuNode $menuNode): self
    {
        $self = new self();

        $self->label = $menuNode->label;
        $self->url = $menuNode->url;
        $self->segment = $menuNode->segment;
        $self->a_target = $menuNode->a_target;
        $self->model_key = $menuNode->model_key;
        $self->order = $menuNode->order;

        return $self;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
//            'parent_segment' => $this->parent_segment,
            'segment' => $this->segment,
            'label' => $this->label,
            'a_target' => $this->a_target,
            'url' => $this->url,
//            'order' => $this->order,
//            'model_key' => $this->model_key,
            'children' => $this->children,
        ];
    }
}
