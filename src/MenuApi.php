<?php

namespace HalcyonLaravelBoilerplate\Menu;

use JsonSerializable;

class MenuApi implements JsonSerializable
{
    public string $name;
    public ?string $description = null;
    /**
     * @var \HalcyonLaravelBoilerplate\Menu\MenuNodeApi[]
     */
    public array $nodes = [];

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'nodes' => $this->nodes,
        ];
    }

    public function asJson($flag = 0)
    {
        return json_encode($this, $flag);
    }

    public function toArray(): array
    {
        return $this->jsonSerialize();
    }
}
