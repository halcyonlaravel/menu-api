<?php

namespace HalcyonLaravelBoilerplate\Menu\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string|null description
 * @property string|null model_key
 * @property int depth
 * @property bool enabled
 * @property string|null template
 * @property string segment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\Menu\Models\MenuNode[] $menuNodes
 * @method static Model create(array $attributes)
 * @method static Builder|Menu whereSegment($value)
 */
class Menu extends Model
{
    protected $fillable = [
        'name',
        'description',
        'model_key',
        'depth',
        'enabled',
        'template',
        'segment',
    ];

    public function getRouteKeyName()
    {
        return 'segment';
    }

    public function getTable()
    {
        return config('menu-api.tables.menus', 'menu_api_menus');
    }

    public function menuNodes()
    {
        return $this->hasMany(config('menu-api.implementations.menu_node', MenuNode::class), 'menu_id');
    }
}
