<?php

namespace HalcyonLaravelBoilerplate\Menu\Models;

use HalcyonLaravelBoilerplate\Menu\Contracts\HasMenuContract;
use HalcyonLaravelBoilerplate\Menu\HasMenuNode;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * @method static create(array $attributes)
 * @property-read \HalcyonLaravelBoilerplate\Menu\Models\Menu $menu
 * @property string|null model_key
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|MenuNode[] $children
 */
class MenuNode extends Model implements HasMenuContract, Sortable
{
    use SortableTrait;

//    public $sortable = [
//        'order_column_name' => 'order_column',
//        'sort_when_creating' => false,
//    ];

    protected $fillable = [
        'menu_id',
        'parent_id',
        'label',
        'url',
        'a_target',
        'segment',
        'model_key',
        'order_column',
    ];

    public function getRouteKeyName()
    {
        return 'segment';
    }

    public function getTable()
    {
        return config('menu-api.tables.menu_nodes', 'menu_api_menu_nodes');
    }

    public function menu()
    {
        return $this->belongsTo(config('menu-api.implementations.menu', Menu::class));
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function scopeParents(Builder $query): Builder
    {
        return $query->where('parent_id', null);
    }


    public function setMenuNode(): HasMenuNode
    {
        return HasMenuNode::create($this)
            ->setSegment($this->segment)
            ->setLabel($this->label)
            ->setModelKey($this->model_key)
            ->setUrl($this->url)
            ->setATarget($this->a_target)
            ->setOrder($this->order_column);
    }
}
