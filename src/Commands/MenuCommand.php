<?php

namespace HalcyonLaravelBoilerplate\Menu\Commands;

use Illuminate\Console\Command;

class MenuCommand extends Command
{
    public $signature = 'menu-api';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
