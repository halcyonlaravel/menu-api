<?php

namespace HalcyonLaravelBoilerplate\Menu;

use Illuminate\Support\Facades\Facade;

/**
 * @mixin MenuManager
 */
class MenuFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return MenuManager::class;
    }
}
