<?php

namespace HalcyonLaravelBoilerplate\Menu;

use Closure;
use ErrorException;
use HalcyonLaravelBoilerplate\Menu\Contracts\HasMenuContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @method HasMenuNode setParentSegment(string $parentSegment)
 * @method HasMenuNode setSegment(string $segment)
 * @method HasMenuNode setModelKey(string $modelKey)
 * @method HasMenuNode setLabel(string $label)
 * @method HasMenuNode setATarget(string $aTarget)
 * @method HasMenuNode setUrl(string $url)
 * @method HasMenuNode setOrder(string $order)
 */
class HasMenuNode extends MenuNodeApi
{
    public Model $model;
    private ?Closure $fetcher;

    public function __construct(Model $model, Closure $fetcher = null)
    {
        parent::__construct();
        $this->model = $model;
        $this->fetcher = $fetcher;
    }

    public static function create(Model $model, Closure $fetcher = null): self
    {
        return new static($model, $fetcher);
    }

    /**
     * @param  string  $modelName
     *
     * @return HasMenuContract[]
     */
    public static function fetch(string $modelName)
    {
        /** @var HasMenuContract|Model $model */
        $model = $modelName::first();
        if (blank($model)) {
            return [];
        }

        $fetcher = $model->setMenuNode()->fetcher;
        if (is_null($fetcher)) {
            return $modelName::all();
        }

        return $fetcher();
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return $this
     * @throws \ErrorException
     */
    public function __call($name, $arguments): self
    {
        if (! in_array(
            $name,
            [
                'setParentSegment',
                'setSegment',
                'setLabel',
                'setATarget',
                'setUrl',
                'setOrder',
                'setModelKey',
            ]
        )) {
            throw new ErrorException(
                sprintf('Invalid %s:%s() in %s.', static::class, $name, $this->model->getMorphClass())
            );
        }

        if (blank($arguments) || ($count = count($arguments) > 1)) {
            throw new ErrorException(
                sprintf(
                    'Expected 1 argument in %s:%s(), %d given in %s.',
                    static::class,
                    $name,
                    $count,
                    $this->model->getMorphClass()
                )
            );
        }

        $argument = $arguments[0];

        if ($name == 'setUrl') {
            $url = $argument;
            if (filter_var($url, FILTER_VALIDATE_URL)) {
                $this->url = rtrim($url, '/');
            } else {
//                $url = self::addBaseUrl($url);

                if (strlen($url) == 0 || strlen($url) > 0 && ($url[0] != '/')) {
                    $url = "/$url";
                }

                $this->url = strlen($url) == 1 ? $url : rtrim($url, '/');
            }
        } else {
            $prop = str_replace('set', '', $name);
            $prop = Str::snake($prop);
            $this->$prop = $argument;
        }

        return $this;
    }

}
