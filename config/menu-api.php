<?php

return [
//    'base_url' => env('MENU_API_BASE_URL', 'http://app.test'),
    'tables' => [
        'menus' => 'menu_api_menus',
        'menu_nodes' => 'menu_api_menu_nodes',
    ],
    'implementations' => [
        'menu' => HalcyonLaravelBoilerplate\Menu\Models\Menu::class,
        'menu_node' => HalcyonLaravelBoilerplate\Menu\Models\MenuNode::class,
    ],
    'models' => [
//      'test-key' => HalcyonLaravelBoilerplate\Menu\Tests\Support\Models\TestModel::class
    ],
];
